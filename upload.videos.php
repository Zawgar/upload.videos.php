<?php
  class uploadVideos {
    // CONFIGURACIÓN general del plugin Upload
    public static $classUpload="controller.videos.php"; // este mismo archivo
    public static $urlHost="http://localhost/_tools/upload.videos.php/"; // raiz del sitio
    public static $dirUpload="uploads/"; // carpeta en la que se van a cargar los archivos / raiz en $urlHost
    public static $indexDigit=4; // cantidad de caracteres reservados para el indice de los archivos
    public static $inputClass="inputFile"; // clase html que identipica a las etiquetas <input>

    public function __construct(){}

    // $modulo(string)                    -> nombre del módulo
    // $id(int)                           -> id registro actual (correcpondiente al módulo)
    // $instancia(string)                 -> nombre de la instancia
    // $format(array/['jpg','png','gif']) -> listado con los formatos permitidos
    // $max(int/1)                        -> cantidad máxima de archivos que se puedan cargar en esta instancia
    public function drawInput($modulo, $id, $instancia, $max=0, $opciones=[]){
      // busca los videos ya cargados para cada instancia
      $initialPreview=$this->loadVideos(self::$dirUpload.$modulo."/".$id."/".$instancia);
      $uploadExtraData=[];

      // ************************
      // HTML *******************
      // ************************
      echo "<div class='form-group'>";
      echo "<input ";
      echo " class='uploadVideos ".self::$inputClass."'";
      echo " name='".$instancia."'";
      echo " data-modulo='".$modulo."'";
      echo " data-id='".$id."'";
      echo " data-instancia='".$instancia."'";
      echo " data-max-file-count='".$max."'";
      echo " data-upload-url='".self::$classUpload."'";
      echo " data-upload-extra-data='".json_encode($uploadExtraData)."'";
      echo " data-initial-preview='".json_encode($initialPreview)."'";
      echo ">";
      echo "</div>";
    }

    // busca los archivos cargados actualmente en el modulo/instancia
    public function loadVideos($dir){
      // CONFIGURACIÓN para cada archivo existente
      $initialPreview=[];

      // Busco si existe un archivo con listado de videos y lo cargo
      if(file_exists($dir.'/videolist.xml')){
        $id=0;
        $xml=simplexml_load_file($dir.'/videolist.xml');
        foreach($xml as $key => $value){
          $initialPreview[$id]=(string)$value;
          $id++;
        }
      }

      return array("initialPreview" => $initialPreview);
    }

    // Upload
    public static function videoUpdate($dir, $videoList, $extraData){
      try{
        // Crea la carpeta si no existe
        if(!file_exists($dir)){
          mkdir($dir, 0777, true);
        }elseif(file_exists($dir.'/videolist.xml')){
          // Elimina el archivo si ya existe
          unlink($dir.'/videolist.xml');
        }

        // Crea el contenido del archivo XML (videolist.xml)
        $xmlString="<?xml version='1.0' encoding='UTF-8'?>";
        $xmlString.="<videoList>";
        foreach($videoList as $key => $value){
          $xmlString.="<url>".$value."</url>";
        }
        $xmlString.='</videoList>';

        // Crea el XML
        $dom=new DOMDocument;
        $dom->preserveWhiteSpace=FALSE;
        $dom->loadXML($xmlString);

        // Guarda el archivo XML
        $dom->save($dir."/videolist.xml");

        //$arrayReturn=array("error" => "No se seleccionó ningún video para cargar u ocurrio un error al enviarlo.");
        $arrayReturn=array("ok" => true);
      }catch(Exception $e){
        $arrayReturn=array("ok" => false, "detalle" => $e->getMessage());
      }

      return $arrayReturn;
    }

  }