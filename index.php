<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8" />
    <title>Upload</title>

    <!--jQuery + UI-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <!--Bootstrap-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />

    <!--PlugIn-->
    <script src="plugin/upload.videos.js"></script>
    <link rel="stylesheet" href="plugin/upload.videos.css" />

    <!--Theme-->
    <script src="plugin/themes/list.theme.js"></script>
    <link rel="stylesheet" href="plugin/themes/list.theme.css" />
  </head>
  <body>
    <h1>Prueba clase</h1>
    <?php
      // Requerido
      require_once("upload.videos.php");

      // Instancia de la Clase
      $objInput=new uploadVideos();

      // Creación de los INPUT
      $objInput->drawInput("paquetes", "38", "videologia", 5);

      //$objInput->drawInput("paquetes", "12", "videologia", 5);
    ?>
    <hr>
    https://vimeo.com/234307900<br>
    https://vimeo.com/234309001<br>
    https://vimeo.com/234370802<br>
    https://vimeo.com/234307103<br>
    https://vimeo.com/234370704<br>
    https://www.youtube.com/watch?v=i84oS8jesCs<br>
    https://www.youtube.com/watch?v=TCwDlmiudp0<br>
    https://www.youtube.com/watch?v=-6Xl9tBWt54
  </body>
</html>
