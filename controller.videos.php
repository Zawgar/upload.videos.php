<?php
  // Require de la clase y PlugIn
  require_once("upload.videos.php");

  // Captura los valores enviados por POST
  foreach($_GET as $get_key => $get_value){
    $$get_key=$get_value;
  }

  // Controlador
  $arrayReturn=array('error' => 'Ocurrio un error inesperado.');
  if(isset($instancia) AND isset($accion)){
    $dir=uploadVideos::$dirUpload.$modulo."/".$id."/".$instancia;
    switch($accion){
      case "load":
        $arrayReturn=uploadVideos::loadVideos($dir);
        break;

      case "update":
        // Validación de información Extra
        if(!isset($extraData)){
          $extraData=[];
        }
        $arrayReturn=uploadVideos::videoUpdate($dir, $videoList, $extraData);
        break;

      default:
        $arrayReturn=array('error' => 'Funciona incorrecta.');
        break;

    }
  }
  echo json_encode($arrayReturn);
  exit;