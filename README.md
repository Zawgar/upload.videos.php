# UPLOAD.video.php

Carga de videos sin la utilización de Base de Datos, soportando Youtube y Vimeo. (apliaremos)

El listado de videos se almacena en un archivo XML en el servidor

El listado de videos es cargado y administrado por el plugin.

Los videos son cargados teniendo en cuenta tres parametros:
- Modulo (noticias, usuarios, inmuebles, etc)
- Id (identificador interno del sistema, un id de registro)
- Instancia (galería, banners, etc)

El plugin genera subcarpetas dentro de una carpeta madre de carga (ej: uploads/) con la siguiente estructura, donde almacena un único archivo XML con el nombre "videolist.xml", ejemplo:
- uploads/noticias/45/recientes/videolist.xml
- uploads/noticias/45/banners/videolist.xml

Utiliza códigos del siguiente repositorio:
https://gist.github.com/yangshun/9892961
