// Template preview de video
function getTemplateVideo(indice, idVideoUploads, url, img){
  template="<div class='video-thumbnail-container' data-id='"+indice+"' data-url='"+url+"'>";
  template+=" <div class='video-thumbnail-img'>"+img+"</div>";
  template+=" <div class='video-thumbnail-controls'>";

  template+=" <div class='video-thumbnail-title'>"+url+"</div>";

  template+="<span class='preview-drag text-info ui-sortable-handle'>";
  template+="<i class='glyphicon glyphicon-resize-vertical'></i>";
  template+="</span>";

  template+="<button type='button' class='btn btn-default btn-xs text-danger preview-delete' data-id='"+indice+"'>";
  template+="<span class='glyphicon glyphicon-trash text-danger' aria-hidden='true'></span>";
  template+="</button>";

  template+="<a href='"+url+"' type='button' class='btn btn-default btn-xs text-primary' target='_blank'>";
  template+="<span class='glyphicon glyphicon-new-window text-primary' aria-hidden='true'></span>";
  template+="</a>";

  template+="<button type='button' class='btn btn-default btn-xs text-warning preview-view' data-url='"+url+"'>";
  template+="<span class='glyphicon glyphicon-zoom-in text-warning' aria-hidden='true'></span>";
  template+="</button>";

  template+=" </div>";
  template+="</div>";

  return template;
}