// Tempalte general
if(!$.isFunction(htmlContent)){
  function htmlContent(idVideoUploads){
    // Inicio
    htmlContent="<div class='video-uploads-container' id='"+idVideoUploads+"'>";

    // Preview
    htmlContent+="<div class='video-preview-thumbnails'>";
    htmlContent+="<div class='video-preview-thumbnails-text'>Listado de Videos</div>";
    htmlContent+="</div>";
    htmlContent+="<div class='video-preview-status text-center text-success'></div>";
    htmlContent+="<div class='video-preview-message alert'></div>";

    // Progress
    htmlContent+="<div class='progress video-preview-progress'>";
    htmlContent+="  <div class='progress-bar progress-bar-success progress-bar-striped active' role='progressbar' aria-valuemin='100' aria-valuemax='100' style='width: 100%;'>";
    htmlContent+="    Cargando...";
    htmlContent+="  </div>";
    htmlContent+="</div>";

    // Controles
    htmlContent+="<div class='input-group video-preview-controles'>";
    htmlContent+="  <input type='text' class='form-control' name='video-preview-input-url' placeholder='Pegue aquí la url del video'>";
    htmlContent+="  <span class='input-group-btn'>";
    htmlContent+="    <button type='button' class='btn btn-primary btn-cargar' title='Agrega un nuevo video a la lista'>";
    htmlContent+="      <span class='glyphicon glyphicon-cloud-upload' aria-hidden='true'></span> Agregar";
    htmlContent+="    </button>";
    htmlContent+="    <button type='button' class='btn btn-success btn-guardar' title='Guarda/Actualiza el listado actual'>";
    htmlContent+="      <span class='glyphicon glyphicon-ok' aria-hidden='true'></span> Guardar";
    htmlContent+="    </button>";
    htmlContent+="  </span>";
    htmlContent+="</div>";

    // Modal POPUP
    htmlContent+="<div class='modal fade' tabindex='-1' role='dialog'>";
    htmlContent+="  <div class='modal-dialog modal-lg'>";
    htmlContent+="    <div class='modal-content'>";
    htmlContent+="      <div class='modal-header'>";
    htmlContent+="        <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>";
    htmlContent+="      </div>";
    htmlContent+="      <div class='modal-body'></div>";
    htmlContent+="    </div>";
    htmlContent+="  </div>";
    htmlContent+="</div>";

    // Cierre
    htmlContent+="</div>";
    return htmlContent;
  }
}

// Template preview de video
if(!$.isFunction(getTemplateVideo)){
  function getTemplateVideo(indice, idVideoUploads, url, img){
    template="<div class='video-thumbnail-container' data-id='"+indice+"' data-url='"+url+"'>";
    template+=" <div class='video-thumbnail-img'>"+img+"</div>";
    template+=" <div class='video-thumbnail-title'>"+url+"</div>";
    template+=" <div class='video-thumbnail-controls'>";
    template+="   <span class='preview-drag text-info' title='Mover / Reordenar'>";
    template+="     <i class='glyphicon glyphicon-move' style='pointer-events: none;'></i>";
    template+="   </span>";
    template+="   <button type='button' class='btn btn-default btn-xs text-danger preview-delete' data-id='"+indice+"'>";
    template+="     <span class='glyphicon glyphicon-trash text-danger' aria-hidden='true'></span>";
    template+="   </button>";
    template+="   <a  href='"+url+"' type='button' class='btn btn-default btn-xs text-primary' target='_blank'>";
    template+="     <span class='glyphicon glyphicon-new-window text-primary' aria-hidden='true'></span>";
    template+="   </a>";
    template+="   <button type='button' class='btn btn-default btn-xs text-warning preview-view' data-url='"+url+"'>";
    template+="     <span class='glyphicon glyphicon-zoom-in text-warning' aria-hidden='true'></span>";
    template+="   </button>";
    template+=" </div>";
    template+="</div>";

    return template;
  }
}

// Agregar/Procesar Video
if(!$.isFunction(addVideo)){
  function addVideo(idVideoUploads, url, thumbArea, listadoVideos, error){
    var videoData=checkUrlVideo(url);

    // Valido que sea un video
    if(videoData==false){
      showMessage(error, "No es una url de video válido.", "danger");
      return false;
    }

    // Busco el último id
    if(Object.keys(listadoVideos).length==0){
      id=0;
    }else{
      id=listadoVideos.length;
      id=listadoVideos[(listadoVideos.length)-1].id+1;
    }

    // Busco la información del video
    var videoInfo=getVideoInfo(videoData.provider, videoData.id);

    // Creo y Agrego el thumb del video
    var videoPreview=getTemplateVideo(id, idVideoUploads, url, videoInfo.img);
    thumbArea.append(videoPreview);

    // Agrego el video
    listadoVideos.push(url);

    // Ordeno el Array de Objetos
    listadoVideos.sort(function(a, b){
      return a.id - b.id;
    });

    return listadoVideos;
  }
}

// Eliminar Video
if(!$.isFunction(delVideo)){
  function delVideo(id, thumbArea, listadoVideos){

    // Elimino el thumb del video
    thumbArea.find(".video-thumbnail-container[data-id='"+id+"']").remove();

    // Elimino el video del listado
    listadoVideos=listadoVideos.filter(function(obj){
      return obj.id!==id;
    });

    return listadoVideos;
  }
}

// Barra de Progreso
if(!$.isFunction(progressShow)){
  function progressShow(progress){
    progress.animate({
      opacity: 0.8,
    }, 200);
  }
}
if(!$.isFunction(progressHide)){
  function progressHide(progress){
    progress.animate({
      opacity: 0,
    }, 100);
  }
}

// Mensajes
if(!$.isFunction(showMessage)){
  function showMessage(error, text, type, time=0){
    hideMessage(error);
    error.addClass("alert-"+type);
    error.html(text).slideDown();
    if(time>0){
      time=time*1000;
      setTimeout(function(){
        hideMessage(error);
      }, time);
    }
  }
}
if(!$.isFunction(hideMessage)){
  function hideMessage(error){
    error.slideUp()
    error.removeClass("alert-success");
    error.removeClass("alert-info");
    error.removeClass("alert-warning");
    error.removeClass("alert-danger");
  }
}

// Validacion de URL de video
if(!$.isFunction(checkUrlVideo)){
  function checkUrlVideo(url){
    if(url==undefined || url==''){ return false; }
    var explodeUrl=url.match(/(http:|https:|)\/\/(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/);

    if(explodeUrl==undefined || url==null){ return false; }
    provider=explodeUrl[3];
    id=explodeUrl[6];

    // no se reconoce como video
    if(provider!=="youtube.com" && provider!=="vimeo.com"){
      return false;
    }

    // si es un video, devuelve los datos del mismo
    return {
      provider: provider,
      id: id
    }
  }
}
if(!$.isFunction(createVideo)){
  function createVideo(provider, id){
    var $iframe=$('<iframe>').addClass("center-block").addClass("embed-responsive-item");
    if(provider=='youtube.com'){
      $iframe.attr('src', '//www.youtube.com/embed/'+id);
    }else if(provider=='vimeo.com'){
      $iframe.attr('src', '//player.vimeo.com/video/'+id);
    }

    $bootVideo="<div class='embed-responsive embed-responsive-16by9'>";
    $bootVideo+=$iframe[0].outerHTML;
    $bootVideo+="</div>";

    return $bootVideo;
  }
}
if(!$.isFunction(getVideoInfo)){
  function getVideoInfo(provider, id){
    var title="";
    var imgUrl="<br><br><span class='glyphicon glyphicon-film btn-lg text-warning' aria-hidden='true'></span>";
    imgUrl+="<br><span class='text-warning'>Preview no disponible</span>";

    // YOUTUBE
    if(provider=='youtube.com'){
      imgUrl="<img src='//img.youtube.com/vi/"+id+"/mqdefault.jpg'>";
    }

    // VIMEO
    if(provider=='vimeo.com'){
      $.ajax({
        method: "GET",
        url: "http://vimeo.com/api/v2/video/"+id+".json",
        async: false
      }).done(function(data){
        if(data.title == null){
          title="";
        }else{
          title=data.title;
        }
        imgUrl="<img src='"+data[0].thumbnail_medium+"'>";
      });
    }
    return {img: imgUrl, title: title};
  }
}

$(document).ready(function(){

  $(".uploadVideos").each(function(){
    obj=$(this);

    // Creo el ID del objeto con un aleatorio para diferenciarlo correctamente
    var addRandName=Math.floor((Math.random()*10000000)+1);
    var idVideoUploads=obj.prop("name")+"_"+addRandName;

    // Parametros enviados por html5 "DATA-"
    obj.modulo=obj.data("modulo");
    obj.id=obj.data("id");
    obj.instancia=obj.data("instancia");
    obj.maxFile=obj.data("max-file-count");
    obj.initialPreview=obj.data("initial-preview");
    obj.urlUpload=obj.data("upload-url");
    obj.urlUploadExtraData=obj.data("upload-extra-data");

    // Array con la información de todos los videos del plugin (indice, url)
    var videoArray=[];

    // Agrego el contenido HTML complementario
    obj.before(htmlContent(idVideoUploads));

    // Oculto el input original
    obj.css("display", "none");

    // Referencia a los objetos
    var thumbArea=$("#"+idVideoUploads).find(".video-preview-thumbnails");
    var thumbAreaText=$("#"+idVideoUploads).find(".video-preview-thumbnails-text");
    var status=$("#"+idVideoUploads).find(".video-preview-status");
    var error=$("#"+idVideoUploads).find(".video-preview-message");
    var progress=$("#"+idVideoUploads).find(".video-preview-progress");
    var inputUrl=$("#"+idVideoUploads).find(".video-preview-controles input[name='video-preview-input-url']");
    var buttonAdd=$("#"+idVideoUploads).find(".video-preview-controles button.btn-cargar");
    var buttonSave=$("#"+idVideoUploads).find(".video-preview-controles button.btn-guardar");
    var modal=$("#"+idVideoUploads).find(".modal");

    // Arreglo con todos los videos cargados en el plugin
    listadoVideos=[];

    // Calcula el ancho de la barra de progreso
    $(window).resize(function(){
      progress.css("width", (inputUrl.width()+25));
    });
    progress.css("width", (inputUrl.width()+25));

    // Boton Agregar
    buttonAdd.on("click", function(){
      progressShow(progress);
      hideMessage(error);
      var url=inputUrl.val().trim();
      inputUrl.val("");

      resultado=addVideo(idVideoUploads, url, thumbArea, listadoVideos, error);

      // Detecto si ocurrio un error al intentar cargar el video
      if(resultado==false){
        progressHide(progress);
        return;
      }

      // Muestro el boton GUARDAR ya que se actualizó el listado
      buttonSave.prop('disabled', false);

      // Bloque el botón AGREGAR si se llegó al máximo de archivos
      if(resultado.length==obj.maxFile){
        buttonAdd.prop('disabled', true);
        showMessage(error, "Límite de videos alcanzado ("+obj.maxFile+").", "warning");
      }

      // Oculto el texto
      thumbAreaText.addClass("none");

      progressHide(progress);
    });

    // Boton Eliminar
    thumbArea.on("click", ".preview-delete", function(){
      hideMessage(error);
      progressShow(progress);
      thumbArea=$(this).closest(".video-preview-thumbnails");
      id=$(this).data("id");

      listadoVideos=delVideo(id, thumbArea, listadoVideos);

      // Detecto si ocurrio un error al intentar cargar el video
      if(listadoVideos===false){
        progressHide(progress);
        return;
      }

      // Muestro el botón GUARDAR ya que se actualizó el listado
      buttonSave.prop('disabled', false);

      // Muestro el botón AGREGAR si la cantidad de videos es menor al limite
      if(listadoVideos.length < obj.maxFile){
        buttonAdd.prop('disabled', false);
      }

      // Muestro el texto
      if(listadoVideos.length==0){
        thumbAreaText.removeClass("none");
      }

      progressHide(progress);
    });

    // Agregar desde el Input (Pegar+Enter)
    inputUrl.keypress(function(e){
      if(e.which==13 && listadoVideos.length < obj.maxFile){
        buttonAdd.click();
      }
    });

    // Abrir Preview
    thumbArea.on("click", ".preview-view", function(){
      vidInfo=checkUrlVideo($(this).data("url"));
      modal.find(".modal-body").html(createVideo(vidInfo.provider, vidInfo.id));
      modal.modal('show');
    });

    // Cerrar Preview
    modal.on('hidden.bs.modal', function(e){
      modal.find(".modal-body").html("");
    })

    // Actualiza el listado
    buttonSave.on("click", function(){
      progressShow(progress);
      var requestUpload=$.ajax({
        url: obj.urlUpload,
        async: false,
        dataType: "json",
        data: {
          accion: 'update',
          modulo: obj.modulo,
          id: obj.id,
          instancia: obj.instancia,
          videoList: listadoVideos,
          extraData: obj.urlUploadExtraData
        }
      });
      requestUpload.done(function(data){
        if(data.ok){
          showMessage(error, "Listado actualizado correctamente.", "success", 3);
        }else{
          showMessage(error, "Ocurrio un error al intentar actualizar el listado.<br>Vuelva a intentarlo en unos minutos.", "warning");
        }
      });
      requestUpload.always(function(){
        progressHide(progress);
        buttonSave.prop("disabled", true);
      });
    });

    // Carga el listado de videos existentes en el servidor
    if(obj.initialPreview['initialPreview'].length>0){
      $.each(obj.initialPreview['initialPreview'], function(key, url){
        addVideo(idVideoUploads, url, thumbArea, listadoVideos)
      });

      // Si la cantidad de videos es la maxima permitida, bloque el boton Agregar
      if(obj.initialPreview['initialPreview'].length==obj.maxFile){
        showMessage(error, "Límite de videos alcanzado ("+obj.maxFile+").", "warning");
        buttonAdd.prop('disabled', true);
      }

      if(obj.initialPreview['initialPreview'].length>0){
        // Oculto el texto
        thumbAreaText.addClass("none");
      }

      buttonSave.prop('disabled', true);
    }

    console.log(listadoVideos);

    // Hago que la lista de videos sean reordenables
    thumbArea.sortable({
      handle: ".preview-drag",
      placeholder: "video-thumbnail-container-highlight",
      opacity : 0.4,
      update: function(event, ui){
        listadoVideos=[];
        thumbArea.children(".video-thumbnail-container").each(function(){
          // Agrego el video
          listadoVideos.push(
            //"id": $(this).data("id"),
            $(this).data("url")
          );
        });

        // Muestro el boton GUARDAR si se actualizó el listado
        buttonSave.prop('disabled', false);
      }
    });

    // Oculto la barra de progreso
    progressHide(progress);

  });

});